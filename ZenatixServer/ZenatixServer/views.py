from django.http import HttpResponse
from django.http import HttpResponseBadRequest
import json
import os
from random import randrange

file_path = "./received_data.csv"


def temperature_data(request):
    raw_data = request.body
    data = None
    try:
        data = json.loads(raw_data)
    except Exception as e:
        print(e)
    if data:
        rand_success = randrange(2)
        if not os.path.isfile(file_path):
            if rand_success:
                with open(file_path, "w") as fd:
                    cols = [col for col in data.keys()]
                    header = ",".join([col for col in cols]) + "\n"
                    values = ",".join([data[col] for col in cols]) + "\n"
                    fd.write(header)
                    fd.write(values)
                return HttpResponse("Ok")
        elif rand_success:
            cols = []
            with open(file_path, "r") as fd:
                cols = fd.read().split("\n")[0].split(",")
            with open(file_path, "a+") as fd:
                values = ",".join([data[col] for col in cols]) + "\n"
                fd.write(values)
            return HttpResponse("Ok")
        HttpResponseBadRequest("FAILED")
    return HttpResponseBadRequest("FAILED")
