import threading
import datetime

def parse_data(file_path):
    data = open(file_path, "r").read()
    lines = data.split("\n")
    header = [value.strip() for value in lines[0].split(",")]
    # print("header",header)
    lines = lines[1:]
    records = []
    for line in lines:
        values = [value.strip() for value in line.split(",")]
        if len(values) == len(header):
            # print("values",values)
            reading = {}
            for col_idx in range(len(header)):
                key = header[col_idx]
                value = values[col_idx]
                reading[key] = value
            records.append(reading)
    return records


def logger(msg):
    print("["+threading.currentThread().getName()+"]", "[" + str(datetime.datetime.now()) + "]", str(msg))
