from Device import Device

total_sent_records = 0
while True:
    print("\n\n****************Starting A new device instance***************\n\n")
    device = Device(transfer_interval_sec=60, buffer_transfer_interval_sec=5)
    device.sent_records += total_sent_records
    device.run()
    total_sent_records = device.num_sent_records()

