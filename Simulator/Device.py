import utils
import requests
import time
import threading
from utils import logger

class BufferedData(object):
    def __init__(self, data_list=None):
        self.data_list = data_list if data_list else []
        self.insert_index = 0
        self.length = 0

    def add_new_record(self, data):
        self.data_list.append(data)
        self.insert_index += 1
        self.length += 1

    def get_next_record(self):
        record = self.data_list[0]
        return record

    def remove_sent_record(self):
        self.data_list = self.data_list[1:]
        self.length -= 1

    def is_empty(self):
        return not self.length

    def get_buffer_data_len(self):
        return self.length


class Device(object):
    def __init__(self, dataset_file_path="dataset.csv", transfer_interval_sec=60, buffer_transfer_interval_sec=5,
                 dest_server_ip="localhost", dest_server_port=8000, api_path="temperature_data"):

        # data set parameters
        self.data_file_path = dataset_file_path
        self.data_set = utils.parse_data(self.data_file_path)
        self.data_iter = iter(self.data_set)

        # transfer parameters
        self.base_url = "http://{}:{}/".format(dest_server_ip, dest_server_port)
        self.dest_url = self.base_url + api_path
        self.transfer_interval_sec = transfer_interval_sec

        # Buffer parameters
        self.buffer = BufferedData()
        self.buffer_transfer_interval_sec = buffer_transfer_interval_sec

        # threading attributes
        self.buffer_thread = None
        self.sent_records = 0

    def run(self):
        self.data_iter = iter(self.data_set)
        while True:
            print("\n")
            logger("Number of Sent Records: {}".format(self.num_sent_records()))
            logger("Number of records in buffer: {}".format(self.num_buffered_records()))
            print("\n")
            try:
                data = next(self.data_iter)
            except StopIteration:
                logger("Live Dataset exhausted")
                break
            logger("\nSending live data: " + str(data))
            success = self.send_data(data)
            if not success:
                logger("Failed in sending: " + str(data))
                logger("Adding data to the buffer\n")
                self.buffer.add_new_record(data)
                if not self.buffer_thread:
                    self.buffer_thread = threading.Thread(target=self.send_buffer_data, daemon=True)
                    self.buffer_thread.start()
            time.sleep(self.transfer_interval_sec)
        if self.buffer_thread:
            logger("Waiting for complete transmission of buffered data")
            self.buffer_thread.join()
        logger("Device Stopped, No More Data To Send.")

    def num_sent_records(self):
        return self.sent_records

    def num_buffered_records(self):
        return self.buffer.get_buffer_data_len()

    def send_buffer_data(self):
        logger("Buffer thread started")
        while not self.buffer.is_empty():
            data = self.buffer.get_next_record()
            logger("\nSending buffered data: " + str(data))
            if self.send_data(data):
                self.buffer.remove_sent_record()
                logger("Sent Buffered Data\n")
            else:
                logger("Failed to send\n")
            time.sleep(self.buffer_transfer_interval_sec)
        self.buffer_thread = None

    def send_data(self, data):
        success = False
        try:
            response = requests.post(self.dest_url, json=data)
            if response.status_code == requests.codes.ok:
                logger("Sent Success\n")
                success = True
                self.sent_records += 1
        except Exception as e:
            pass
            # logger(e.__repr__())
        return success
