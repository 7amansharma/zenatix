# Summary #

The edge program is under the folder Simulator in the project directory.

The Server program is a django project under the folder ZenatixServer in the main project directory.

### Project setup ###

Install the dependencies using requirements.txt file in the main project directory

### How to run the edge program ###

Go to the main project directory and run following commands

1) cd Simulator

2) python3/python simulator.py

The simulator should start with verbose logs of what is happening.

### How to run the Server program ###

From main project directory, run following commands


1) cd ZenatixServer

2) python manage.py runserver

### Configuration guidelines for edge program ###

The simulator script uses a Device class whose constructor 
accepts following optional parameters with default values.

#### dataset_file_path="dataset.csv"
    location of the dataset.csv file should be same as the Device.py file.

#### transfer_interval_sec=60
    The time interval between consecutive data read and transfer of real time data.

#### buffer_transfer_interval_sec=5
    The time interval between consecutive data read and tansfer of buffered data.

#### dest_server_ip="localhost"
    please change according to the server settings

#### dest_server_port=8000
    please change according to the server settings

#### api_path="temperature_data"
    Please change according to the server settings.

